package com.kfwebstandard.jpajsfmasterdetailexample.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ken
 */
@Entity
@Table(name = "surgical", catalog = "HOSPITALDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "Surgical.findAll", query = "SELECT s FROM Surgical s"),
    @NamedQuery(name = "Surgical.findById", query = "SELECT s FROM Surgical s WHERE s.id = :id"),
    @NamedQuery(name = "Surgical.findByDateofsurgery", query = "SELECT s FROM Surgical s WHERE s.dateofsurgery = :dateofsurgery"),
    @NamedQuery(name = "Surgical.findBySurgery", query = "SELECT s FROM Surgical s WHERE s.surgery = :surgery"),
    @NamedQuery(name = "Surgical.findByRoomfee", query = "SELECT s FROM Surgical s WHERE s.roomfee = :roomfee"),
    @NamedQuery(name = "Surgical.findBySurgeonfee", query = "SELECT s FROM Surgical s WHERE s.surgeonfee = :surgeonfee"),
    @NamedQuery(name = "Surgical.findBySupplies", query = "SELECT s FROM Surgical s WHERE s.supplies = :supplies")})
public class Surgical implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATEOFSURGERY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateofsurgery;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "SURGERY")
    private String surgery;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ROOMFEE")
    private BigDecimal roomfee;
    @Column(name = "SURGEONFEE")
    private BigDecimal surgeonfee;
    @Column(name = "SUPPLIES")
    private BigDecimal supplies;
    @JoinColumn(name = "PATIENTID", referencedColumnName = "PATIENTID")
    @ManyToOne
    private Patient patientid;

    public Surgical() {
    }

    public Surgical(Integer id) {
        this.id = id;
    }

    public Surgical(Integer id, Date dateofsurgery, String surgery) {
        this.id = id;
        this.dateofsurgery = dateofsurgery;
        this.surgery = surgery;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateofsurgery() {
        return dateofsurgery;
    }

    public void setDateofsurgery(Date dateofsurgery) {
        this.dateofsurgery = dateofsurgery;
    }

    public String getSurgery() {
        return surgery;
    }

    public void setSurgery(String surgery) {
        this.surgery = surgery;
    }

    public BigDecimal getRoomfee() {
        return roomfee;
    }

    public void setRoomfee(BigDecimal roomfee) {
        this.roomfee = roomfee;
    }

    public BigDecimal getSurgeonfee() {
        return surgeonfee;
    }

    public void setSurgeonfee(BigDecimal surgeonfee) {
        this.surgeonfee = surgeonfee;
    }

    public BigDecimal getSupplies() {
        return supplies;
    }

    public void setSupplies(BigDecimal supplies) {
        this.supplies = supplies;
    }

    public Patient getPatientid() {
        return patientid;
    }

    public void setPatientid(Patient patientid) {
        this.patientid = patientid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Surgical)) {
            return false;
        }
        Surgical other = (Surgical) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kfstandard.jpajsfmasterdetailexample.Surgical[ id=" + id + " ]";
    }

}
