package com.kfwebstandard.jpajsfmasterdetailexample.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ken
 */
@Entity
@Table(name = "medication", catalog = "HOSPITALDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "Medication.findAll", query = "SELECT m FROM Medication m"),
    @NamedQuery(name = "Medication.findById", query = "SELECT m FROM Medication m WHERE m.id = :id"),
    @NamedQuery(name = "Medication.findByDateofmed", query = "SELECT m FROM Medication m WHERE m.dateofmed = :dateofmed"),
    @NamedQuery(name = "Medication.findByMed", query = "SELECT m FROM Medication m WHERE m.med = :med"),
    @NamedQuery(name = "Medication.findByUnitcost", query = "SELECT m FROM Medication m WHERE m.unitcost = :unitcost"),
    @NamedQuery(name = "Medication.findByUnits", query = "SELECT m FROM Medication m WHERE m.units = :units")})
public class Medication implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATEOFMED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateofmed;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "MED")
    private String med;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UNITCOST")
    private BigDecimal unitcost;
    @Column(name = "UNITS")
    private BigDecimal units;
    @JoinColumn(name = "PATIENTID", referencedColumnName = "PATIENTID")
    @ManyToOne
    private Patient patientid;

    public Medication() {
    }

    public Medication(Integer id) {
        this.id = id;
    }

    public Medication(Integer id, Date dateofmed, String med) {
        this.id = id;
        this.dateofmed = dateofmed;
        this.med = med;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateofmed() {
        return dateofmed;
    }

    public void setDateofmed(Date dateofmed) {
        this.dateofmed = dateofmed;
    }

    public String getMed() {
        return med;
    }

    public void setMed(String med) {
        this.med = med;
    }

    public BigDecimal getUnitcost() {
        return unitcost;
    }

    public void setUnitcost(BigDecimal unitcost) {
        this.unitcost = unitcost;
    }

    public BigDecimal getUnits() {
        return units;
    }

    public void setUnits(BigDecimal units) {
        this.units = units;
    }

    public Patient getPatientid() {
        return patientid;
    }

    public void setPatientid(Patient patientid) {
        this.patientid = patientid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medication)) {
            return false;
        }
        Medication other = (Medication) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kfstandard.jpajsfmasterdetailexample.Medication[ id=" + id + " ]";
    }

}
