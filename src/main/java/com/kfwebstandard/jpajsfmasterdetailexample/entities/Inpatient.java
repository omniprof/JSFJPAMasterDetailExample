package com.kfwebstandard.jpajsfmasterdetailexample.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ken
 */
@Entity
@Table(name = "inpatient", catalog = "HOSPITALDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "Inpatient.findAll", query = "SELECT i FROM Inpatient i"),
    @NamedQuery(name = "Inpatient.findById", query = "SELECT i FROM Inpatient i WHERE i.id = :id"),
    @NamedQuery(name = "Inpatient.findByDateofstay", query = "SELECT i FROM Inpatient i WHERE i.dateofstay = :dateofstay"),
    @NamedQuery(name = "Inpatient.findByRoomnumber", query = "SELECT i FROM Inpatient i WHERE i.roomnumber = :roomnumber"),
    @NamedQuery(name = "Inpatient.findByDailyrate", query = "SELECT i FROM Inpatient i WHERE i.dailyrate = :dailyrate"),
    @NamedQuery(name = "Inpatient.findBySupplies", query = "SELECT i FROM Inpatient i WHERE i.supplies = :supplies"),
    @NamedQuery(name = "Inpatient.findByServices", query = "SELECT i FROM Inpatient i WHERE i.services = :services")})
public class Inpatient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATEOFSTAY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateofstay;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "ROOMNUMBER")
    private String roomnumber;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DAILYRATE")
    private BigDecimal dailyrate;
    @Column(name = "SUPPLIES")
    private BigDecimal supplies;
    @Column(name = "SERVICES")
    private BigDecimal services;
    @JoinColumn(name = "PATIENTID", referencedColumnName = "PATIENTID")
    @ManyToOne
    private Patient patientid;

    public Inpatient() {
    }

    public Inpatient(Integer id) {
        this.id = id;
    }

    public Inpatient(Integer id, Date dateofstay, String roomnumber) {
        this.id = id;
        this.dateofstay = dateofstay;
        this.roomnumber = roomnumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateofstay() {
        return dateofstay;
    }

    public void setDateofstay(Date dateofstay) {
        this.dateofstay = dateofstay;
    }

    public String getRoomnumber() {
        return roomnumber;
    }

    public void setRoomnumber(String roomnumber) {
        this.roomnumber = roomnumber;
    }

    public BigDecimal getDailyrate() {
        return dailyrate;
    }

    public void setDailyrate(BigDecimal dailyrate) {
        this.dailyrate = dailyrate;
    }

    public BigDecimal getSupplies() {
        return supplies;
    }

    public void setSupplies(BigDecimal supplies) {
        this.supplies = supplies;
    }

    public BigDecimal getServices() {
        return services;
    }

    public void setServices(BigDecimal services) {
        this.services = services;
    }

    public Patient getPatientid() {
        return patientid;
    }

    public void setPatientid(Patient patientid) {
        this.patientid = patientid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inpatient)) {
            return false;
        }
        Inpatient other = (Inpatient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kfstandard.jpajsfmasterdetailexample.Inpatient[ id=" + id + " ]";
    }

}
