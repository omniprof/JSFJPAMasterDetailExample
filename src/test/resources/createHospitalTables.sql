USE HOSPITALDB;

DROP TABLE IF EXISTS inpatient;
DROP TABLE IF EXISTS surgical;
DROP TABLE IF EXISTS medication;
DROP TABLE IF EXISTS patient;

CREATE TABLE patient (
  patientid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  lastname VARCHAR(30) NOT NULL DEFAULT '',
  firstname VARCHAR(30) NOT NULL DEFAULT '',
  diagnosis VARCHAR(60) NOT NULL DEFAULT '',
  admissiondate TIMESTAMP DEFAULT 0,
  releasedate TIMESTAMP DEFAULT 0
);


CREATE TABLE inpatient (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  patientid INT,
  dateofstay TIMESTAMP DEFAULT 0,
  roomnumber VARCHAR(5) NOT NULL DEFAULT '',
  dailyrate DECIMAL(10,2),
  supplies DECIMAL(10,2),
  services DECIMAL(10,2),
  KEY patientid (patientid),
  CONSTRAINT patient_1 FOREIGN KEY (patientid) REFERENCES patient (patientid)
) ENGINE=InnoDB;


CREATE TABLE surgical (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  patientid INT,
  dateofsurgery TIMESTAMP DEFAULT 0,
  surgery VARCHAR(25) NOT NULL DEFAULT '',
  roomfee DECIMAL(10,2),
  surgeonfee DECIMAL(10,2),
  supplies DECIMAL(10,2),
  KEY patientid (patientid),
  CONSTRAINT patient_2 FOREIGN KEY (patientid) REFERENCES patient (patientid)
) ENGINE=InnoDB;


CREATE TABLE medication (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  patientid INT,
  dateofmed TIMESTAMP DEFAULT 0,
  med VARCHAR(45) NOT NULL DEFAULT '',
  unitcost DECIMAL(10,2),
  units DECIMAL(10,2) ,
  KEY patientid (patientid),
  CONSTRAINT patient_3 FOREIGN KEY (patientid) REFERENCES patient (patientid)
) ENGINE=InnoDB;

INSERT INTO patient (lastname, firstname, diagnosis, admissiondate, releasedate) VALUES
("Wayne","Bruce","Asthma","2014-01-23 9:00:00","2014-01-25 13:00:00"),
("Allen","Barry","Kidney Stones","2014-02-18 9:00:00","2014-02-21 13:00:00"),
("Kent","Clark","Tonsilitis","2014-05-02 9:00:00","2014-05-07 13:00:00"),
("Stark","Tony","Appendicitis","2014-07-14 9:00:00","2014-07-15 13:00:00"),
("Banner","Bruce","Gall Bladder Disease","2014-11-09 9:00:00","2014-11-12 13:00:00");

INSERT INTO inpatient(patientid,dateofstay,roomnumber,dailyrate,supplies,services) VALUES
(1, "2014-01-23 9:00:00","A1","250.00","75.24","12.95"),
(1, "2014-01-24 9:00:00","A1","250.00","90.15","58.12"),
(1, "2014-01-25 9:00:00","A1","250.00","120.23","87.05"),
(2, "2014-02-18 9:00:00","B2","150.00","120.23","87.05"),
(2, "2014-02-19 9:00:00","B2","150.00","120.23","87.05"),
(2, "2014-02-20 9:00:00","B2","150.00","120.23","87.05"),
(2, "2014-02-21 9:00:00","B2","150.00","120.23","87.05"),
(3, "2014-05-02 9:00:00","C3","150.00","120.23","87.05"),
(3, "2014-05-03 9:00:00","C3","150.00","120.23","87.05"),
(3, "2014-05-04 9:00:00","C3","150.00","120.23","87.05"),
(3, "2014-05-05 9:00:00","C3","150.00","120.23","87.05"),
(3, "2014-05-06 9:00:00","C3","150.00","120.23","87.05"),
(3, "2014-05-07 9:00:00","C3","150.00","120.23","87.05"),
(3, "2014-05-07 9:00:00","C3","150.00","120.23","87.05"),
(4, "2014-07-14 9:00:00","D4","150.00","120.23","87.05"),
(4, "2014-07-15 9:00:00","C4","150.00","120.23","87.05"),
(5, "2014-11-09 9:00:00","E5","150.00","120.23","87.05"),
(5, "2014-11-10 9:00:00","E5","150.00","120.23","87.05"),
(5, "2014-11-11 9:00:00","E5","150.00","120.23","87.05"),
(5, "2014-11-12 9:00:00","E5","150.00","120.23","87.05");

INSERT INTO surgical(patientid,dateofsurgery,surgery,roomfee,surgeonfee,supplies) VALUES
(1, "2014-01-24 11:00:00","Lung Transplant", 2500.12, 4200.00, 934.23),
(1, "2014-02-24 09:00:00","Finger Transplant", 2500.12, 4200.00, 934.23),
(1, "2014-03-24 10:00:00","Toe Transplant", 2500.12, 4200.00, 934.23),
(2, "2014-02-19 07:00:00","Kidney Transplant", 2500.12, 4200.00, 934.23),
(3, "2014-05-03 21:00:00","Tonsil Transplant", 2500.12, 4200.00, 934.23),
(4, "2014-07-14 14:00:00","Appendix Transplant", 2500.12, 4200.00, 934.23),
(5, "2014-11-11 10:00:00","Gall Bladder Transplant", 2500.12, 4200.00, 934.23);

INSERT INTO medication(patientid,dateofmed,med,unitcost,units) VALUES
(1, "2014-01-24 11:00:00", "Snickers", 1.25, 5.0),
(2, "2014-02-19 07:00:00", "M and M", 1.10, 15.0),
(3, "2014-05-03 21:00:00", "O Henry", 2.49, 11.0),
(4, "2014-07-14 14:00:00", "Caramilk", 4.23, 3.0),
(5, "2014-11-11 10:00:00", "Aero Bar", 9.43, 19.0);
